require 'sinatra'
require 'i18n'
require './boundaries/application/bill_api'

before do
  I18n.load_path = Dir[File.join(settings.root, 'locales', '*.yml')]
  I18n.locale = :'en-GB'
end

set :root, File.dirname(__FILE__)
set :bind, '0.0.0.0'

get '/' do
  begin
    @bill = Application::BillApi.get_bill
    erb :bill
  rescue  Exception => e
    @description = e.message
    erb :error
  end
end
