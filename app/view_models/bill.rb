require 'i18n'
require 'money'

module App
  module ViewModel
    class Bill
      def initialize(bill)
        @bill = bill
      end

      def total
        money(@bill.total)
      end

      def due_date
        i18n_l(statement.due, format: :short)
      end

      def subscriptions
        @subscriptions ||= package.subscriptions.map do |subscription|
          { name: subscription.name, cost: money(subscription.cost) }
        end
      end

      def calls
        @calls ||= call_charges.calls.map do |call|
          {
            called: call.called,
            duration: call.duration,
            cost: money(call.cost)
          }
        end
      end

      def call_charges_total
        @call_charges_total ||= money(call_charges.total)
      end

      def subscriptions_total
        money(package.total)
      end

      def sky_store_rentals
        @rentals ||= sky_store.rentals.map(&method(:additional_item))
      end

      def sky_store_buy_and_keep
        @buy_and_keep ||= sky_store.buy_and_keep.map(&method(:additional_item))
      end

      def sky_store_total
        money(sky_store.total)
      end

      private

      def money(value)
        Money.new(format_currency(value), i18n_t('number.currency.code')).format
      end

      def format_currency(value)
        (value * 100).to_i
      end

      def i18n_t(key, opts = {})
        I18n.t(key, { default: key }.merge(opts))
      end

      def i18n_l(key, opts = nil)
        I18n.l(key, opts)
      end

      def additional_item(item)
        { title: item.title, cost: money(item.cost) }
      end

      def statement
        @statement ||= @bill.statement
      end

      def package
        @package ||= @bill.package
      end

      def sky_store
        @sky_store ||= @bill.sky_store
      end

      def call_charges
        @call_charges ||= @bill.call_charges
      end
    end
  end
end
