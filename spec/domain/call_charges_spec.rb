require 'rspec'
require './domain/call_charges'

describe CallCharges do
  it 'sets and gets calls' do
    subject = described_class.new
    calls = [double('call_1'), double('call_2')]

    expect{ subject.calls = calls }.to change(subject, :calls).from(nil).to(calls)
  end

  it 'sets and gets total attribute' do
    subject = described_class.new
    total = 59.64

    expect{ subject.total = total }.to change(subject, :total).from(nil).to(59.64)
  end
end