require 'rspec'
require './domain/subscription'

describe Subscription do
  it 'gets and sets type attribute' do
    subject = Subscription.new

    subject.type = 'broadband'

    expect(subject.type).to eq 'broadband'
  end

  it 'gets and sets name attribute' do
    subject = Subscription.new

    subject.name = 'Fibre Unlimited'

    expect(subject.name).to eq 'Fibre Unlimited'
  end

  it 'gets and sets cost attribute' do
    subject = Subscription.new

    subject.cost = 16.40

    expect(subject.cost).to eq 16.40
  end
end