require 'rspec'
require './domain/sky_store'

describe SkyStore do

  it 'sets and gets rentals attribute' do
    subject = described_class.new
    rentals = [double('rental1'), double('rental2')]

    expect{ subject.rentals = rentals }.to change(subject, :rentals).from(nil).to(rentals)
  end

  it 'sets and gets buy_and_keep attribute' do
    subject = described_class.new
    items = [double('item_1'), double('item_2')]

    expect{ subject.buy_and_keep = items }.to change(subject, :buy_and_keep).from(nil).to(items)
  end

  it 'sets and gets total attribute' do
    subject = described_class.new

    expect{ subject.total = 24.97 }.to change(subject, :total).from(nil).to(24.97)
  end
end