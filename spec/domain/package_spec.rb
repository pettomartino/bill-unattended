require 'rspec'
require './domain/package'

describe Package do
  it 'sets and gets total attributes' do
    subject = described_class.new

    expect { subject.total = 71.40 }.to change(subject, :total).from(nil).to(71.40)
  end

  it 'sets and gets subscription attributes' do
    subject = described_class.new
    subscriptions = [double('subscriptiosn'), double('subscriptiosn')]

    expect { subject.subscriptions = subscriptions }.to change(subject, :subscriptions).from(nil).to(subscriptions)
  end
end