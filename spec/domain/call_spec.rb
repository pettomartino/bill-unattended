require 'rspec'
require './domain/call'

describe Call do
  it 'should sets and gets called' do
    subject = Call.new

    subject.called = '07716393769'

    expect(subject.called).to eq '07716393769'
  end

  it 'sets and gets duration' do
    subject = Call.new

    subject.duration = '00:23:03'

    expect(subject.duration).to eq '00:23:03'
  end

  it 'sets and gets cost' do
    subject = Call.new

    subject.cost = 2.13

    expect(subject.cost).to eq 2.13
  end
end
