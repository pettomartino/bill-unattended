require 'rspec'
require './domain/period'

describe Period do
  it 'sets and gets from period attribute' do
    subject = described_class.new
    date = Date.new(2015, 01, 01)
    subject.from = date

    expect(subject.from).to eq date
  end

  it 'sets and gets from period attribute' do
    subject = described_class.new
    date = Date.new(2015, 01, 01)
    subject.to = date


    expect(subject.to).to eq date
  end
end