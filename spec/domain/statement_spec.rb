require 'rspec'
require './domain/statement'
require './domain/period'

describe Statement do
  it 'sets and gets generated attribute' do
    subject = described_class.new
    date = Date.strptime('2015-01-11')
    subject.generated = date

    expect(subject.generated).to eq date
  end

  it 'sets and gets due attribute' do
    subject = described_class.new
    date = Date.strptime('2015-01-11')

    subject.due = date

    expect(subject.due).to eq date
  end

  it 'sets and gets period attribute' do
    subject = described_class.new
    period = double('period')

    expect { subject.period = period }.to change(subject, :period).from(nil).to(period)
  end
end