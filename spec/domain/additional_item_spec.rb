require 'rspec'
require './domain/additional_item'

describe AdditionalItem do
  it 'sets and gets the title' do
    subject = AdditionalItem.new

    subject.title = "That's what she said"

    expect(subject.title).to eq "That's what she said"
  end

  it 'sets and gets the cost' do
    subject = AdditionalItem.new

    subject.cost = 9.99

    expect(subject.cost).to eq 9.99
  end
end
