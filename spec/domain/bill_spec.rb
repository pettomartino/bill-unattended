require 'rspec'
require './domain/bill'

describe Bill do
  it 'sets and gets statement attribute' do
    subject = described_class.new
    statement_double = double('statement')

    expect{ subject.statement = statement_double}.to change(subject, :statement).from(nil).to(statement_double)
  end

  it 'sets and gets package' do
    subject = described_class.new
    package_double = double('package')

    expect { subject.package = package_double }.to change(subject, :package).from(nil).to(package_double)
  end

  it 'sets and gets skyStore attribute' do
    subject = described_class.new
    sky_store_double = double('sky_store')

    expect { subject.sky_store = sky_store_double }.to change(subject, :sky_store).from(nil).to(sky_store_double)
  end

  it 'sets and gets callCharges attribute' do
    subject = described_class.new
    call_charges = double('callCharges')

    expect { subject.call_charges = call_charges }.to change(subject, :call_charges).from(nil).to(call_charges)
  end

  it 'sets and gets total attribute' do
    subject = described_class.new

    expect { subject.total = 136.03 }.to change(subject, :total).from(nil).to(136.03)
  end
end