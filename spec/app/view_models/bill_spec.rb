require 'rspec'
require './app/view_models/bill'

describe App::ViewModel::Bill do

  it 'returns total formatted' do
    subject = described_class.new(double('bill', total: 123.12))

    expect(subject.total).to eq '£123.12'
  end

  it 'returns due_date formatted' do
    statement = double('statement', due: Date.new(2015, 1, 25))
    bill = double('bill', statement: statement)
    subject = described_class.new(bill)

    expect(subject.due_date).to eq '25 Jan'
  end

  it 'returns subscriptions' do
    package = double('package', subscriptions: subscriptions)
    bill = double('bill', package: package)
    subject = described_class.new(bill)

    expect(subject.subscriptions.size).to eq 3
    expect(subject.subscriptions.first).to include(name: 'foo bar', cost: '£1.40')
  end

  it 'returns subscriptions total' do
    package = double('package', total: 2.90)
    bill = double('bill', package: package)
    subject = described_class.new(bill)

    expect(subject.subscriptions_total).to eq '£2.90'
  end

  it 'returns skyStore rentals' do
    sky_store = double('sky_store', rentals: sky_store_rentals)
    bill = double('bill', sky_store: sky_store)
    subject = described_class.new(bill)

    rentals = subject.sky_store_rentals

    expect(rentals.size).to eq 1
    expect(rentals.first).to include(title: 'foo bar', cost: '£1.23')
  end

  it 'returns skyStore keyAndKeep' do
    sky_store = double('sky_store', buy_and_keep: sky_store_buy_and_keep)
    bill = double('bill', sky_store: sky_store)
    subject = described_class.new(bill)

    buy_and_keep = subject.sky_store_buy_and_keep

    expect(buy_and_keep.size).to eq 2
    expect(buy_and_keep.first).to include(title: 'foo bar', cost: '£1.12')
  end

  it 'returns skyStore total' do
    sky_store = double('sky_store', total: 24.97)
    bill = double('bill', sky_store: sky_store)
    subject = described_class.new(bill)

    sky_store_total = subject.sky_store_total

    expect(sky_store_total).to eq '£24.97'
  end

  it 'returns calls' do
    call_charges = double('call_charges', calls: calls)
    bill = double('bill', call_charges: call_charges)
    subject = described_class.new(bill)

    calls = subject.calls

    expect(calls.first).to include(called: "07716393769", duration: "00:23:03", cost: '£2.13')
  end

  it 'returns call charges total' do
    call_charges = double('call_charges', total: 2.13)
    bill = double('bill', call_charges: call_charges)
    subject = described_class.new(bill)

    expect(subject.call_charges_total).to eq '£2.13'
  end

  def calls
    [
      double('calls', called: "07716393769", duration: "00:23:03", cost: 2.13),
      double('calls', called: "07716393769", duration: "00:23:03", cost: 2.13)
    ]
  end

  def sky_store_rentals
    [
      double('additional_item', title: 'foo bar', cost: 1.23)
    ]
  end

  def sky_store_buy_and_keep
    [
      double('additional_item-1', title: 'foo bar', cost: 1.12),
      double('additional_item-2', title: 'foo', cost: 0.22)
    ]
  end

  def subscriptions
    [
      double('subscript-1', name: 'foo bar', cost: 1.40),
      double('subscript-2', name: 'foo', cost: 0.40),
      double('subscript-3', name: 'loren ipsum', cost: 1.10),
    ]
  end
end