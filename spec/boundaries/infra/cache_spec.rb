require 'rspec'
require './boundaries/infra/cache'

describe Infra::Cache do
    it 'returns content from cache' do
      client = spy('client')
      allow(client).to receive(:get).with('key').and_return('content')
      subject = described_class.new(client: client, ttl: 120)


      expect(subject.get('key')).to eq 'content'
    end

    it 'will set cache on client' do
      client = spy('client')

      subject = described_class.new(client: client, ttl: 120)
      subject.set('key', 'content')

      expect(client).to have_received(:setex).with('key', 120, 'content')
    end

end