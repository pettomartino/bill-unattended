require 'rspec'
require './boundaries/infra/bill_http'

describe Infra::BillHttp do

  context 'when cache is enabled' do
    context 'and there is no cache content' do
      it 'returns content from http requester' do
        base_url = 'http://safe-plains-5453.herokuapp.com'
        cache = double('cache')
        http_response = double('http_response', body: 'content')
        http_requester = double('http_requester')

        allow(http_requester).to receive(:get).with("#{base_url}/bill.json").and_return(http_response)
        allow(cache).to receive(:set).with("#{base_url}/bill.json", 'content')
        allow(cache).to receive(:get).with("#{base_url}/bill.json").and_return(nil)

        subject = described_class.new(base_url: base_url,
                                      http_requester: http_requester,
                                      cache: cache,
                                      cache_enable: true)

        result = subject.get('/bill.json')

        expect(result).to eq 'content'
      end
    end

    context 'when there is cache' do
      it 'returns content from cache' do
        base_url = 'http://safe-plains-5453.herokuapp.com'
        cache = double('cache')
        http_requester = double('http_requester')
        allow(cache).to receive(:get).with("#{base_url}/bill.json").and_return('cached-content')

        subject = described_class.new(cache: cache,
                                      base_url: base_url,
                                      http_requester: http_requester,
                                      cache_enable: true)

        result = subject.get('/bill.json')

        expect(result).to eq 'cached-content'
      end
    end
  end

  context 'when cache is disabled' do
    it 'returns content from http requester' do
      base_url = 'http://safe-plains-5453.herokuapp.com'
      cache = spy('cache')
      http_response = double('http_response', body: 'content')
      http_requester = double('http_requester')
      allow(http_requester).to receive(:get).with("#{base_url}/bill.json").and_return(http_response)

      subject = described_class.new(base_url: base_url,
                                    http_requester: http_requester,
                                    cache: cache,
                                    cache_enable: false)

      result = subject.get('/bill.json')

      expect(cache).not_to have_received(:get)
      expect(result).to eq 'content'
    end
  end
end