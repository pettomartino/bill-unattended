require 'rspec'
require './boundaries/application/representers/call_charges_representer'
require './domain/call'

describe Representers::CallChargesRepresenter do

  let(:call_charges) { described_class.new(CallCharges.new).from_json(call_charges_json) }

  context 'call charges parser' do
    it 'should parse total' do
      expect(call_charges.total).to eq 59.64
    end
  end

  context 'call parser' do
    it 'should all be kind of Call' do
      expect(call_charges.calls).to all (be_an(Call))
    end

    it 'should return called value' do
      call = call_charges.calls.first

      expect(call.called).to eq '07716393769'
    end

    it 'should return duration value' do
      call = call_charges.calls.first

      expect(call.duration).to eq '00:23:03'
    end

    it 'should return cost value' do
      call = call_charges.calls.first

      expect(call.cost).to eq 2.13
    end
  end

  def call_charges_json
    bill_json['callCharges'].to_json
  end

  def bill_json
    JSON.parse(File.read('spec/support/fixtures/bill.json'))
  end
end