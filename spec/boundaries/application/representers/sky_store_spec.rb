require 'rspec'
require './boundaries/application/representers/sky_store_representer'
require './domain/sky_store'

describe Representers::SkyStoreRepresenter do

  let(:sky_store) { described_class.new(SkyStore.new).from_json(sky_store_json) }

  context 'rentals parsing' do
    it 'should be an AdditionalItem' do
      rentals = sky_store.rentals

      expect(rentals).to all( be_an(AdditionalItem) )
      expect(rentals.size).to eq 1
    end

    it 'should have AdditionalItem attributes' do
      rental = sky_store.rentals.first

      expect(rental.title).to eq '50 Shades of Grey'
      expect(rental.cost).to eq 4.99
    end
  end

  context 'buyAndKeep parsing' do
    it 'should be an AdditionalItem' do
      buy_and_keep = sky_store.buy_and_keep

      expect(buy_and_keep).to all( be_an(AdditionalItem) )
      expect(buy_and_keep.size).to eq 2
    end

    it 'should have AdditionalItem attributes' do
      item = sky_store.buy_and_keep.first

      expect(item.title).to eq "That's what she said"
      expect(item.cost).to eq 9.99
    end
  end

  it 'should parse total' do
    total = sky_store.total

    expect(total).to eq 24.97
  end

  def sky_store_json
    bill_json['skyStore'].to_json
  end

  def bill_json
    JSON.parse(File.read('spec/support/fixtures/bill.json'))
  end
end