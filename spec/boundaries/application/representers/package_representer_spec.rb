require 'rspec'
require 'json'
require './boundaries/application/representers/package_representer'

describe Representers::PackageRepresenter do

  let(:package) { described_class.new(Package.new).from_json(package_json) }

  it 'should parse and coerce total' do
    expect(package.total).to eq 71.40
  end

  it 'should parse subscriptions' do
    expect(package.subscriptions).to all( be_an(Subscription) )
  end

  it 'should parse subscription attributes' do
    subscription = package.subscriptions.first

    expect(subscription.type).to eq 'tv'
    expect(subscription.name).to eq 'Variety with Movies HD'
    expect(subscription.cost).to eq 50.00
  end

  def package_json
    bill_json['package'].to_json
  end

  def bill_json
    JSON.parse(File.read('spec/support/fixtures/bill.json'))
  end
end