require 'rspec'
require './boundaries/application/representers/bill_representer'

describe Representers::BillRepresenter do

  let(:bill) { described_class.new(Bill.new).from_json(bill_json) }

  it 'returns total' do
    expect(bill.total).to eq 136.03
  end

  it 'returns statement' do
    expect(bill.statement).to be_kind_of(Statement)
  end

  it 'returns package' do
    expect(bill.package).to be_kind_of(Package)
  end

  it 'returns callCharges' do
    expect(bill.call_charges).to be_kind_of(CallCharges)
  end

  it 'returns skyStore' do
    expect(bill.sky_store).to be_kind_of(SkyStore)
  end

  def bill_json
    File.read('spec/support/fixtures/bill.json')
  end
end