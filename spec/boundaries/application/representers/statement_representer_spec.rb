require 'rspec'
require './boundaries/application/representers/statement_representer'
require './domain/statement'
require './boundaries/application/representers/statement_representer'
require 'json'

describe Representers::StatementRepresenter do
  let(:statement) { described_class.new(Statement.new).from_json(statement_json) }

  it 'should parse and coerce generated' do
    expect(statement.generated).to eq Date.new(2015, 1, 11)
  end

  it 'should parse and coerce due' do
    expect(statement.generated).to eq Date.new(2015, 1, 11)
  end

  it 'should parse and coerce period' do
    period = statement.period

    expect(period.from).to eq Date.new(2015, 1, 26)
    expect(period.to).to eq Date.new(2015, 2, 25)
  end

  def statement_json
    bill_json['statement'].to_json
  end

  def bill_json
    JSON.parse(File.read('spec/support/fixtures/bill.json'))
  end
end