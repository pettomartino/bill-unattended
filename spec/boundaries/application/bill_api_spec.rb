require 'rspec'
require './boundaries/application/bill_api'
require './app/view_models/bill'

describe Application::BillApi do
  it 'returns a Bill ViewModel' do
    bill_http = double('bill_http', get: bill_json)

    result = described_class.get_bill(bill_http: bill_http)

    expect(result).to be_kind_of(App::ViewModel::Bill)
    expect(result.total).to eq '£136.03'
  end

  def bill_json
    File.read('spec/support/fixtures/bill.json')
  end
end