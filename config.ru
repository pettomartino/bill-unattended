require 'dotenv'
Dotenv.load

require './app/app.rb'
run Sinatra::Application
