require './boundaries/infra/bill_http'
require_relative 'representers/bill_representer'
require './app/view_models/bill'
require './domain/bill'
require_relative 'errors'

module Application
  class BillApi
    def self.get_bill(bill_http: ::Infra::BillHttp.new)
      bill_json = bill_http.get('/bill.json')
      App::ViewModel::Bill.new(bill_representer(json: bill_json))
    rescue
      raise Application::BillApiError
    end

    private_class_method

    def self.bill_representer(json:)
      ::Representers::BillRepresenter.new.from_json(json)
    rescue
      raise Application::BillRepresenterError
    end
  end
end
