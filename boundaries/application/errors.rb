module Application
  BillApiError = Class.new(::StandardError)
  BillRepresenterError = Class.new(::StandardError)
end
