require 'representable/json'
require 'representable/coercion'
require './domain/period'

module Representers
  class StatementRepresenter < Representable::Decorator
    include Representable::JSON
    include Representable::Coercion

    property :generated, type: Date
    property :due, type: Date

    property :period, class: Period do
      property :from, type: Date
      property :to, type: Date
    end
  end
end
