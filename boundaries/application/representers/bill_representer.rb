require 'representable/json'
require 'representable/coercion'
require_relative 'call_charges_representer'
require_relative 'statement_representer'
require_relative 'package_representer'
require_relative 'sky_store_representer'
require './domain/statement'
require './domain/call_charges'
require './domain/sky_store'
require './domain/package'
require './domain/bill'

module Representers
  class BillRepresenter < Representable::Decorator
    include Representable::JSON
    include Representable::Coercion

    def initialize(entity = Bill.new)
      super(entity)
    end

    property :statement, decorator: Representers::StatementRepresenter, class: Statement
    property :package, decorator: PackageRepresenter, class: Package
    property :call_charges, as: :callCharges, decorator: Representers::CallChargesRepresenter, class: CallCharges
    property :sky_store, as: :skyStore, decorator: Representers::SkyStoreRepresenter, class: SkyStore
    property :total, type: Float
  end
end
