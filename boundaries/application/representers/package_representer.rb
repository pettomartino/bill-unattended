require 'representable/json'
require 'representable/coercion'
require './domain/subscription'

module Representers
  class PackageRepresenter < Representable::Decorator
    include Representable::JSON
    include Representable::Coercion

    property :total, type: Float
    collection :subscriptions, class: Subscription do
      property :type, type: String
      property :name, type: String
      property :cost, type: Float
    end
  end
end
