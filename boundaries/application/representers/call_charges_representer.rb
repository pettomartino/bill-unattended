require 'representable/json'
require 'representable/coercion'
require './domain/call'

module Representers
  class CallChargesRepresenter < Representable::Decorator
    include Representable::JSON
    include Representable::Coercion

    collection :calls, class: Call do
      property :called, type: String
      property :duration, type: String
      property :cost, type: Float
    end

    property :total, type: Float
  end
end
