require 'representable/json'
require 'representable/coercion'
require './domain/additional_item'

module Representers
  class SkyStoreRepresenter < Representable::Decorator
    include Representable::JSON
    include Representable::Coercion

    property :total, type: Float

    collection :rentals, class: AdditionalItem do
      property :title, type: String
      property :cost, type: Float
    end

    collection :buy_and_keep, as: :buyAndKeep, class: AdditionalItem do
      property :title, type: String
      property :cost, type: Float
    end
  end
end
