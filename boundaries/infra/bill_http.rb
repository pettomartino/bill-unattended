require_relative 'cache'
require 'typhoeus'

module Infra
  class BillHttp
    def initialize(overrides = {})
      @http_requester = overrides.fetch(:http_requester) { ::Typhoeus }
      @base_url = overrides.fetch(:base_url) { ENV['SERVICE_BASE_URL'] }
      @cache_enable = overrides.fetch(:cache_enable) { ENV['CACHE_ENABLE'] }
      config_cache(overrides[:cache])
    end

    def get(path)
      url = "#{@base_url}#{path}"
      cache_content = get_cache(url)

      return cache_content if cache_content

      response = @http_requester.get(url).body
      set_cache(url, response)
      response
    end

    private

    def config_cache(cache)
      return unless @cache_enable
      @cache = cache || Cache.new
    end

    def get_cache(key)
      return unless @cache_enable
      @cache&.get(key)
    end

    def set_cache(key, content)
      return unless @cache_enable
      @cache&.set(key, content)
    end
  end
end
