require 'redis'

module Infra
  class Cache
    def initialize(overrides = {})
      @client = overrides.fetch(:client) { Redis.new(url: ENV['REDIS_URL']) }
      @ttl = overrides.fetch(:ttl) { ENV['REDIS_TTL'].to_i }
    end

    def get(cache_key)
      @client.get(cache_key)
    end

    def set(cache_key, response)
      @client.setex(cache_key, @ttl, response)
    end
  end
end
